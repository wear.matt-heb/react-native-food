import React from 'react';
import {View, TextInput, StyleSheet} from 'react-native';
import {Feather} from '@expo/vector-icons';

const SearchBar = ({term, onTermChange, onTermSubmit}) => {

    return (
    <View style={styles.backgroundStyle}>
        <Feather name="search" style={styles.iconStyles} />
        <TextInput 
            placeholder="search" 
            style={styles.inputStyle}
            autoCapitalize="none"
            autoCorrect={false}
            onChangeText={onTermChange} 
            onEndEditing={onTermSubmit}
        />
    </View>
    );
};

const styles = StyleSheet.create({
    backgroundStyle: {
        marginTop: 16, 
        marginBottom: 16,
        backgroundColor: '#F0EEEE',
        height: 50,
        borderRadius: 5, 
        marginHorizontal: 16,
        flexDirection:'row',
        alignContent: 'flex-start'
    },
    inputStyle: {
        borderColor: 'black',
        borderWidth: 0,
        flex: 1
    },
    iconStyles: {
        fontSize:35,
        alignSelf: 'center',
        marginHorizontal: 15
    }
});

export default SearchBar;