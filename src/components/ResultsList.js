import React from 'react';
import {View, Text, StyleSheet, FlatList, TouchableOpacity} from 'react-native';
import ResultsDetail from './ResultsDetail';
import {withNavigation} from 'react-navigation';

const ResultsList = ({title, results, navigation}) => {
    if (!results.length) {
        return null;
    }

    return (
    <View style={styles.container}>
        <Text style={styles.title}>{title}</Text>
        <FlatList  
            horizontal={true}
            data={results}
            keyExtractor={result => results.id}
            showsHorizontalScrollIndicator={false}
            renderItem={({item}) => {
                return (
                    <TouchableOpacity onPress={() => navigation.navigate('ResultsShow', { id: item.id })}> 
                        <ResultsDetail result={item} />
                    </TouchableOpacity>
                );
            }}
        />
    </View>
    );
};

const styles = StyleSheet.create({
    container: {
        marginBottom: 16
    },  
    title: {
        fontSize: 18,
        fontWeight: 'bold',
        marginLeft: 16
    }
});

export default withNavigation(ResultsList);